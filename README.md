# Docker Client

Simple docker app that uses docker client to start/stop containers.

``` sh
//Build test image
docker build -t static_app .

base64 image_example/docker-compose.yml

// RUN
base64 image_example/docker-compose.yml | go run main.go
```