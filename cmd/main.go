package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"gitlab.com/jhsc/docker-client/api"
	"gitlab.com/jhsc/docker-client/config"
	"gitlab.com/jhsc/docker-client/dockker"
)

var (
	logger       = log.New(os.Stdout, "", log.LstdFlags|log.LUTC)
	useEnvConfig = flag.Bool("e", false, "use environment variables as config")
)

func main() {
	flag.Usage = help
	flag.Parse()

	// TODO: global configs?

	commands := map[string]func(){
		"start":   startServer,
		"init":    initConfig,
		"gen-key": genKey,
		"deploy":  deploy,
		"help":    help,
	}

	if cmdFunc, ok := commands[flag.Arg(0)]; ok {
		cmdFunc()
	} else {
		help()
		//  Misuse of shell builtins
		os.Exit(2)
	}
}

func initConfig() {
	if _, err := os.Stat(config.ConfigFile); !os.IsNotExist(err) {
		logger.Fatalf("configuration file already exists: %s", config.ConfigFile)
	}

	logger.Printf("creating initial configuration: %s", config.ConfigFile)

	cfg, err := config.Init()
	if err != nil {
		logger.Fatalf("failed to generate initial configuration: %s", err)
	}

	err = ioutil.WriteFile(config.ConfigFile, []byte(cfg), 0666)
	if err != nil {
		logger.Fatalf("failed to write configuration file: %s", err)
	}

}

func startServer() {
	cfg, err := config.GetConfig(*useEnvConfig)
	if err != nil {
		logger.Fatalf("failed to load configuration: %s", err)
	}
	baseURL, err := url.Parse(cfg.BaseURL)
	if err != nil {
		logger.Fatalf("failed to parse base url: %s", err)
	}

	endpoint := "unix:///var/run/docker.sock"
	composeEncoded := "wrowkrhjkweqhry2rhjkrhjkwherjkhwejkrhjkwerjkhwjkerhjkwe="

	data := dockker.DeployerPayload{
		Project: "Test App",
		Registry: dockker.Registry{
			URL:      "private_url",
			Login:    "username",
			Password: "password",
		},
		ComposeFile: composeEncoded,
		Extra:       map[string]string{"TAG": "hello"},
	}

	ds, err := dockker.NewService(endpoint, data)
	if err != nil {
		logger.Fatalf("failed to create new docker service: %s", err)
	}

	// logger.Printf("baseurl: %s", baseURL)
	apiHandler := api.New(&api.Config{
		DockerService: ds,
	})

	router := chi.NewRouter()
	router.Use(middleware.RequestLogger(&middleware.DefaultLogFormatter{Logger: logger}))
	router.Use(middleware.Recoverer)

	router.Mount("/api/v1", apiHandler)
	// router.NotFound(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
	// 	w.WriteHeader(http.StatusNotFound)
	// }))

	logger.Printf("starting the server: %s", cfg.Address)
	if err := http.ListenAndServe(cfg.Address, http.StripPrefix(baseURL.Path, router)); err != nil {
		logger.Fatalf("listen and serve failed: %v", err)
	}
}

func genKey() {
	logger.Printf("key: %s", config.GenKeyHex(32))
}

func deploy() {
	// endpoint := "unix:///var/run/docker.sock"
	// //  Read from stdin e.i base64 image_example/docker-compose.yml | MAIN
	// var composeEncoded string
	// fmt.Scanf("%s", &composeEncoded)
	// fmt.Printf("composeEncoded = %s\n", composeEncoded)

	// if len(composeEncoded) == 0 {
	// 	log.Fatal(errors.New("empty compose file"))
	// }
	// //
	// data := service.DeployerPayload{
	// 	Project: "Test App",
	// 	Registry: service.Registry{
	// 		URL:      "private_url",
	// 		Login:    "username",
	// 		Password: "password",
	// 	},
	// 	ComposeFile: composeEncoded,
	// 	Extra:       map[string]string{"TAG": "hello"},
	// }

	// ds, err := service.NewDeployService(endpoint, data)
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// err = ds.DeployCompose()
	// if err != nil {
	// 	log.Fatal(err)
	// }
}

func help() {
	fmt.Fprintln(os.Stderr, `Usage:
	deployer start                      - start the server
	deployer init                       - create an initial configuration file
	deployer gen-key                    - generate a random 32-byte hex-encoded key
	deployer help                       - show this message
Use -e flag to read configuration from environment variables instead of a file. E.g.:
	deployer -e start`)
}
