dep:
	go mod vendor

# Use this only for development
dev:
	go build -o deployer cmd/main.go
	./deployer

test:
	go test ./... -coverprofile cp.out
	go tool cover -html=cp.out